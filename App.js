import React from "react";
import { StyleSheet,Text,TextInput,View,Image,FlatList ,KeyboardAvoidingView,Platform,Modal, Button} from "react-native";
import { useState } from "react";

export default function App(){

  const [state,setState] = useState(true)

  const Change = () =>{
    setState(!state)
  }

  return(
    <>
    <View>
      <Text>Bye Suhas</Text>
    <View style={styles.container}>
    <Modal visible ={state}>
      <Text>Hello Suhas</Text>
      <Button onPress={Change} title="X"></Button>
    </Modal>
    </View>
    <Button title="<-" onPress={Change}></Button>
    </View>
    </>

  );
}

const styles = StyleSheet.create({
  container:{
    backgroundColor:'grey',
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  input:{
    backgroundColor:'white',
    marginBottom:20
  },
  img:{
    marginTop:20,
    marginBottom:20
  }
  
})

